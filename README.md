# Chimney

This is socks5 proxy with encryption, you can use it to access youtube etc.


# Version

[current] 0.5

1. support AES-gcm and ChaCha20
2. base on socks5 protocol
3. user config.json to set up the user parameter


**Note you have to have the vps or vm(except china).**


# How to build
change folder to src, then


1. go get github.com/Evan2698/chimney/cmd/client **or**  go get github.com/Evan2698/chimney/cmd/server
2. go build github.com/Evan2698/chimney/cmd/client
3. go build github.com/Evan2698/chimney/cmd/server



## License

And of course:

MIT: https://opensource.org/licenses/MIT


