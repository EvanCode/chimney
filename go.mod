module github.com/Evan2698/chimney

go 1.12

require (
	github.com/Evan2698/android-netstack v0.0.0-20190101110143-3a7a9258406d // indirect
	github.com/lucas-clemente/quic-go v0.11.2
	github.com/miekg/dns v1.1.14 // indirect
)
